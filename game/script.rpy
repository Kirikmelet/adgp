﻿
define R = 'Renai'
define Y = 'Yuki'
transform slightleft:
    xalign -0.4
    yalign 1.0

label start:
   "This game was created with Ren'Py!"
   "Property of Troy D."
   "Build 2.1: Yuki emotion test"
   call ch01
   return